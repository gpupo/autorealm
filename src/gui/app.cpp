/**********************************************************************************
 *autorealm - A vectorized graphic editor to create maps, mostly for RPG games    *
 *Copyright (C) 2012-2013 Morel Bérenger                                          *
 *                                                                                *
 *This file is part of autorealm.                                                 *
 *                                                                                *
 *    autorealm is free software: you can redistribute it and/or modify           *
 *    it under the terms of the GNU Lesser General Public License as published by *
 *    the Free Software Foundation, either version 3 of the License, or           *
 *    (at your option) any later version.                                         *
 *                                                                                *
 *    autorealm is distributed in the hope that it will be useful,                *
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
 *    GNU Lesser General Public License for more details.                         *
 *                                                                                *
 *    You should have received a copy of the GNU Lesser General Public License    *
 *    along with autorealm.  If not, see <http://www.gnu.org/licenses/>.          *
 **********************************************************************************/

#include "app.h"

#include <exception>
#include <wx/splash.h>
#include <boost/filesystem.hpp>
#include "appconfig.h"
#include "mainframe.h"

IMPLEMENT_APP(App)

bool App::OnInit()
{
	bool wxsOK = true;

	try
	{
		wxInitAllImageHandlers();

		if(wxsOK)
		{
			std::string splash(AppConfig::buildPath(AppConfig::SPLASH));
			if(splash.empty())
				m_app = new MainFrame(0);
			else
			{
				wxImage image;
				std::string splashFile(AppConfig::buildPath(AppConfig::SPLASH));
#ifndef NDEBUG
				printf("%s\n",splashFile.c_str());
#endif
				image.LoadFile(splashFile);
				if(image.IsOk())
				{
					std::unique_ptr<wxSplashScreen> splash (new wxSplashScreen(image, wxSPLASH_CENTRE_ON_SCREEN|wxSPLASH_NO_TIMEOUT, 0,NULL,wxID_ANY));
					wxAppConsole::Yield(true);

					m_app = new MainFrame(0);

					splash.reset();
				}
				else
					m_app = new MainFrame(0);
			}
			m_app->Show();
			SetTopWindow(m_app);
		}
	}
	catch(std::exception &e)
	{
		wxsOK = false;
		wxMessageBox(e.what(), "Fatal exception");
	}

	return wxsOK;
}

