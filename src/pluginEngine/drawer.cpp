/**********************************************************************************
 *autorealm - A vectorized graphic editor to create maps, mostly for RPG games    *
 *Copyright (C) 2012 Morel Bérenger                                               *
 *                                                                                *
 *This file is part of autorealm.                                                 *
 *                                                                                *
 *    autorealm is free software: you can redistribute it and/or modify           *
 *    it under the terms of the GNU Lesser General Public License as published by *
 *    the Free Software Foundation, either version 3 of the License, or           *
 *    (at your option) any later version.                                         *
 *                                                                                *
 *    autorealm is distributed in the hope that it will be useful,                *
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
 *    GNU Lesser General Public License for more details.                         *
 *                                                                                *
 *    You should have received a copy of the GNU Lesser General Public License    *
 *    along with autorealm.  If not, see <http://www.gnu.org/licenses/>.          *
 **********************************************************************************/

#include "drawer.h"

#include <renderEngine/shape.h>
#include <gui/renderwindow.h>
#include <pluginEngine/renderer.h>
#include <pluginEngine/drawerlist.h>

Drawer::Drawer(RenderWindow *window, std::unique_ptr<Renderer> r, Render::TagList const &tags)
:Plugin(window)
,m_shape()
,m_selectedRenderer(std::move(r))
,m_tagList(tags)
{
	m_selectedRenderer->m_parent=this;
	DrawerList::GetInstance().m_drawerList.push_back(this);///\todo fix the situation of unloaded plug-in. Not an emergency, because it is currently not possible to do such stuff
}

Drawer::Drawer(Drawer const& other)
:Plugin(other)
,m_shape(other.m_shape)
,m_selectedRenderer(other.m_selectedRenderer->clone())
{
}

Drawer::~Drawer(void)throw()
{
}

void Drawer::addPoint(wxMouseEvent &event)
{
	addVertex(Render::Point(event.GetX(),event.GetY(),0));
	render();
}

void Drawer::addVertex(Render::Point const &p)
{
	m_shape.addVertex(p, *m_selectedRenderer);///\warning is the correct renderer used for Border?
}

void Drawer::moveMouse(wxMouseEvent &event)
{
	m_shape.pop();
	addPoint(event);
}

void Drawer::render(void)
{
	m_target->startRendering();
	m_shape.draw();
	m_target->draw();
	m_target->finalizeRendering();
}

Drawer::operator Renderer&(void)
{
	return *m_selectedRenderer;
}

bool Drawer::operator==(Render::TagList const& tag)const
{
	return tag==m_tagList;
}

void Drawer::createShape(void)
{
	assert(m_shape.empty());
	m_shape.setFiller(m_target->getFiller());
}

Render::TagList const Drawer::getTags(void)const
{
	return m_tagList;
}

Renderer *Drawer::create(Render::TagList const& tag)const
{
	Renderer *tmp=m_selectedRenderer->clone();
	tmp->init(tag);
	return tmp;
}
